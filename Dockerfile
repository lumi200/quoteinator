FROM gradle:jdk17-focal as build
RUN mkdir quoteinator
COPY . /quoteinator
WORKDIR /quoteinator
RUN gradle clean bootJar

FROM docker.io/openjdk:17-alpine
RUN addgroup --system spring && adduser -S -s /bin/false -G spring  spring
RUN mkdir quoteinator
COPY --from=build /quoteinator/build/libs/*.jar /quoteinator/bot.jar
WORKDIR /quoteinator
RUN chown -R spring:spring /quoteinator
USER spring
ENTRYPOINT ["java","-jar","/quoteinator/bot.jar"]