package at.lumi200.discordquotebot.listener;

import at.lumi200.discordquotebot.commands.ChatInputCommand;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.Event;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;

@Component
public class ChatInputCommandListener {

	private final Collection<ChatInputCommand> commands;


	public ChatInputCommandListener(Collection<ChatInputCommand> commands, GatewayDiscordClient client) {
		this.commands = commands;
		client.on(ChatInputInteractionEvent.class,this::handle).subscribe();
	}

	public Mono<Void> handle(ChatInputInteractionEvent event) {
		return Flux.fromIterable(commands)
				.filter(command -> command.getName().equals(event.getCommandName()))
				.next()
				.flatMap(command -> command.handle(event));
	}
}

