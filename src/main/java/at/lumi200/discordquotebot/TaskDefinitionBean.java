package at.lumi200.discordquotebot;

import at.lumi200.discordquotebot.entity.Quote;
import at.lumi200.discordquotebot.service.QuoteService;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.User;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.RestClient;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

@Log4j2
@Service
public class TaskDefinitionBean implements Runnable {

	private TaskDefinition taskDefinition;

	private final RestClient client;

	private final GatewayDiscordClient gatewayDiscordClient;
	private final QuoteService quoteService;

	private Long serverId;

	public TaskDefinitionBean(RestClient client, GatewayDiscordClient gatewayDiscordClient, QuoteService quoteService) {
		this.client = client;
		this.gatewayDiscordClient = gatewayDiscordClient;
		this.quoteService = quoteService;
	}

	@Override
	public void run() {

		Quote quote = quoteService.getRandom(serverId);
		Hibernate.initialize(quote);
		if (quote != null) {
			User user = new User(gatewayDiscordClient, client.getUserService().getUser(quote.getSource().getSnowflake()).block());

			DateTimeFormatter formatter = new DateTimeFormatterBuilder()
					.appendPattern("HH:mm dd.MM.yyyy")
					.toFormatter().withZone(ZoneId.of("Europe/Vienna"));

			EmbedCreateSpec embed;


			if (quote.getTarget() == null)
				embed = EmbedCreateSpec.builder()
						.author(user.getUsername(), "", user.getAvatarUrl())
						.title(quote.getQuote())
						.description("%s".formatted(quote.getCreated().format(formatter)))
						.build();
			else {
				User target = new User(gatewayDiscordClient, client.getUserService().getUser(quote.getTarget().getSnowflake()).block());

				embed = EmbedCreateSpec.builder()
						.author(user.getUsername(), "", user.getAvatarUrl())
						.title(quote.getQuote())
						.description("zu %s%n%s".formatted(target.getUsername(), quote.getCreated().format(formatter)))
						.build();
			}
			taskDefinition.getChannel().createMessage(embed).block();
		}
	}

	public TaskDefinition getTaskDefinition() {
		return taskDefinition;
	}

	public void setTaskDefinition(TaskDefinition taskDefinition) {
		this.taskDefinition = taskDefinition;
	}

	public Long getServerId() {
		return serverId;
	}

	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}
}
