package at.lumi200.discordquotebot;

import at.lumi200.discordquotebot.entity.ScheduledQuote;
import at.lumi200.discordquotebot.entity.Server;
import at.lumi200.discordquotebot.repo.ScheduledQuoteRepository;
import at.lumi200.discordquotebot.service.QuoteService;
import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.rest.RestClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ScheduledFuture;

@Service
@Log4j2
public class TaskDefinitionService {
	@Autowired
	private TaskScheduler taskScheduler;
	@Autowired
	private GatewayDiscordClient gatewayDiscordClient;
	@Autowired
	private ScheduledQuoteRepository scheduleRepository;
	Map<String, ScheduledFuture<?>> jobMap = new HashMap<>();

	public void scheduleATask(UUID jobId, Runnable tasklet, String cronExpression, Server server, long channelSnowflake) {


		ScheduledFuture<?> scheduledTask = taskScheduler.schedule(tasklet, new CronTrigger(cronExpression, TimeZone.getTimeZone(TimeZone.getDefault().getID())));
		scheduleRepository.save(new ScheduledQuote(jobId, channelSnowflake, cronExpression, server));
		jobMap.put(String.valueOf(jobId), scheduledTask);
	}

	public void removeScheduledTask(UUID jobId) {
		ScheduledFuture<?> scheduledTask = jobMap.get(String.valueOf(jobId));
		if (scheduledTask != null) {
			scheduledTask.cancel(true);
			scheduleRepository.deleteById(jobId);
			jobMap.put(String.valueOf(jobId), null);
		}
	}


	public void rescheduleAllTasks(RestClient client, QuoteService QuoteService) {
		List<ScheduledQuote> tasks = (List<ScheduledQuote>) scheduleRepository.findAll();
		CronDescriptor descriptor = CronDescriptor.instance(Locale.UK);
		CronParser parser = new CronParser(CronDefinitionBuilder.instanceDefinitionFor(CronType.SPRING));

		tasks.stream().forEach((task) -> {
			TaskDefinitionBean taskDefinitionBean = new TaskDefinitionBean(client, gatewayDiscordClient, QuoteService);
			taskDefinitionBean.setTaskDefinition(new TaskDefinition(gatewayDiscordClient.getChannelById(Snowflake.of(task.getSnowflake())).cast(MessageChannel.class).block()));
			taskDefinitionBean.setServerId(client.getChannelById(Snowflake.of(task.getSnowflake())).getData().block().guildId().get().asLong());
			log.debug(task.toString());
			taskScheduler.schedule(taskDefinitionBean, new CronTrigger(task.getCron()));
			log.debug("Registered Task Server: {} Cron: {}", task.getServer().getSnowflake().longValue(), descriptor.describe(parser.parse(task.getCron())));
		});
	}
}
