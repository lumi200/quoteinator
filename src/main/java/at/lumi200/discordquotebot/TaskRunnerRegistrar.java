package at.lumi200.discordquotebot;

import at.lumi200.discordquotebot.repo.QuoteRepository;
import at.lumi200.discordquotebot.service.QuoteService;
import discord4j.rest.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class TaskRunnerRegistrar implements ApplicationRunner {

	private TaskDefinitionService taskDefinitionService;

	private RestClient client;

	private QuoteService quoteService;

	public TaskRunnerRegistrar(TaskDefinitionService taskDefinitionService, RestClient client, QuoteService quoteService) {
		this.taskDefinitionService = taskDefinitionService;
		this.client = client;
		this.quoteService = quoteService;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		taskDefinitionService.rescheduleAllTasks(client,quoteService);
	}
}
