package at.lumi200.discordquotebot.service;

import at.lumi200.discordquotebot.entity.Quote;
import at.lumi200.discordquotebot.repo.QuoteRepository;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Service
@Log4j2
public class QuoteServiceImpl implements QuoteService {

	private QuoteRepository quoteRepository;

	public QuoteServiceImpl(QuoteRepository quoteRepository) {
		this.quoteRepository = quoteRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public Quote getRandom(Long serverId) {
		List<Quote> quoteList = quoteRepository.findByServer_Snowflake(serverId);
		if (quoteList.size()>0)
		return quoteList.get(ThreadLocalRandom.current().nextInt(0, quoteList.size()));
		return null;
	}

	@Override
	public Quote getRandomFromSource(UUID sourceId, Long serverId) {
		List<Quote> quoteList = quoteRepository.findByServer_SnowflakeAndSource_Id(serverId, sourceId);
		return quoteList.get(ThreadLocalRandom.current().nextInt(0, quoteList.size()));
	}

	@Override
	public List<Quote> getAllFromSource(UUID sourceId) {
		return quoteRepository.findBySource_Id(sourceId);
	}

	@Override
	public List<Quote> getAllFromServer(Long serverId) {
		return quoteRepository.findByServer_SnowflakeOrderByCreatedAsc(serverId);
	}

	@Override
	public Quote add() {
		return null;
	}
}
