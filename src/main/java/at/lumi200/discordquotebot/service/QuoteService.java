package at.lumi200.discordquotebot.service;

import at.lumi200.discordquotebot.entity.Quote;

import java.util.List;
import java.util.UUID;

public interface QuoteService {

	public Quote getRandom(Long serverId);

	public Quote getRandomFromSource(UUID sourceId, Long serverId);

	public List<Quote> getAllFromSource(UUID sourceId);

	public List<Quote> getAllFromServer(Long serverId);
	public Quote add();


}
