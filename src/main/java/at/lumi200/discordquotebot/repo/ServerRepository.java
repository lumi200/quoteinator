package at.lumi200.discordquotebot.repo;

import at.lumi200.discordquotebot.entity.Server;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ServerRepository extends CrudRepository<Server, UUID> {
	boolean existsBySnowflake(Long snowflake);
	Server findBySnowflake(Long snowflake);



}