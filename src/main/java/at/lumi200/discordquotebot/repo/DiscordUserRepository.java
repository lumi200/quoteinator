package at.lumi200.discordquotebot.repo;

import at.lumi200.discordquotebot.entity.DiscordUser;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface DiscordUserRepository extends CrudRepository<DiscordUser, Long> {
	boolean existsBySnowflake(Long discordId);
	DiscordUser findByName(String name);

	DiscordUser findBySnowflake(long snowflake);
}