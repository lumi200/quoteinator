package at.lumi200.discordquotebot.repo;

import at.lumi200.discordquotebot.entity.Quote;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface QuoteRepository extends CrudRepository<Quote, UUID> {
	//TODO Implement a better way to get a random quote with native querry and mapping
	@EntityGraph(value = "graph.quote.creator", type = EntityGraph.EntityGraphType.LOAD)
	List<Quote> findByServer_SnowflakeAndSource_Id(Long id, UUID id1);

	@EntityGraph(value = "graph.quote.creator",type = EntityGraph.EntityGraphType.LOAD)
	List<Quote> findByServer_Snowflake(Long id);

	List<Quote> findByServer_SnowflakeOrderByCreatedAsc(Long snowflake);



//	@Query(value = "SELECT * FROM quote q WHERE source_id = ?1 ORDER BY RANDOM() LIMIT 1", nativeQuery = true)
//	Quote findRandomBySource(UUID sourceId);

//	@Query(value = "SELECT * FROM quote q ORDER BY RANDOM() LIMIT 1", nativeQuery = true)
//	Quote findRandom();


	List<Quote> findBySource_Id(UUID source);


}