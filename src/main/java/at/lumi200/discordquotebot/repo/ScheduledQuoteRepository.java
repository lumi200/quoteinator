package at.lumi200.discordquotebot.repo;

import at.lumi200.discordquotebot.entity.ScheduledQuote;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ScheduledQuoteRepository extends CrudRepository<ScheduledQuote, UUID> {
	void deleteById(UUID jobId);
}