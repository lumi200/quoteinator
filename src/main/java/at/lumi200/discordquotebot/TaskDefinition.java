package at.lumi200.discordquotebot;

import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.entity.channel.MessageChannel;
import lombok.Data;

@Data
public class TaskDefinition {
	private MessageChannel channel;


	public TaskDefinition(MessageChannel channel) {
		this.channel = channel;
	}
}
