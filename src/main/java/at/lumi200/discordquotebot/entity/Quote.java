package at.lumi200.discordquotebot.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@Table(name = "quote")
@AllArgsConstructor
@NamedEntityGraphs(
		@NamedEntityGraph(name = "graph.quote.creator", attributeNodes = @NamedAttributeNode(value = "creator"))
)
public class Quote {

	public Quote(String quote, Server server, DiscordUser creator, DiscordUser source) {
		this.quote = quote;
		this.server = server;
		this.creator = creator;
		this.source = source;
	}

	public Quote(String quote, Server server, DiscordUser creator, DiscordUser source, DiscordUser target) {
		this.quote = quote;
		this.server = server;
		this.creator = creator;
		this.source = source;
		this.target = target;
	}

	public Quote(String quote) {
		this.quote = quote;
	}

	//TODO find better way than to use FetchType.EAGER
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	@Column(name = "id", nullable = false)
	private UUID id;

	@Column(name = "created")
	private LocalDateTime created;

	@Column(name = "quote")
	private String quote;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "server_id", nullable = false)
	private Server server;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn()
	private DiscordUser creator;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "source_id", nullable = false)
	private DiscordUser source;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "target_id")
	private DiscordUser target;

	public Quote() {

	}
}