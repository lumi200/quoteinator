package at.lumi200.discordquotebot.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "discord_user")
public class DiscordUser {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	@Column(name = "id", nullable = false)
	private UUID id;

	@Column(name = "snowflake")
	private Long snowflake;

	@Column(name = "name")
	private String name;


	public DiscordUser(Long snowflake) {
		this.snowflake = snowflake;
	}

	public DiscordUser(String name) {
		this.name = name;
	}
	public DiscordUser(){

	}



	@OneToMany(mappedBy = "creator")
	private Set<Quote> createdQuotes = new LinkedHashSet<>();

	@OneToMany(mappedBy = "source")
	private Set<Quote> sourceOfQuotes = new LinkedHashSet<>();

	@OneToMany(mappedBy = "target")
	private Set<Quote> targetedQuotes = new LinkedHashSet<>();

}