package at.lumi200.discordquotebot.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "server")
public class Server {

	//ALTER TABLE public."server" ADD quote_channel int8 NULL;
	@Column(name = "quote_channel")
	public Long quoteChannel;
	@Id
	@Column(name = "snowflake")
	private Long snowflake;

	public Server(Long snowflake) {
		this.snowflake = snowflake;
	}


	public Server() {
	}
}