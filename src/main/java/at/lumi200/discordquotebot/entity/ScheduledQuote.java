package at.lumi200.discordquotebot.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "scheduled_quote")
public class ScheduledQuote {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	@Column(name = "id", nullable = false)
	private UUID id;

	@Column(name = "snowflake")
	private Long snowflake;

	@Column(name = "cron")
	private String cron;

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ScheduledQuote{");
		sb.append("id=").append(id);
		sb.append(", snowflake=").append(snowflake);
		sb.append(", cron='").append(cron).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "server_id", nullable = false)
	private Server server;




}