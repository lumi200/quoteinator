package at.lumi200.discordquotebot.commands;

import at.lumi200.discordquotebot.entity.DiscordUser;
import at.lumi200.discordquotebot.entity.Quote;
import at.lumi200.discordquotebot.entity.Server;
import at.lumi200.discordquotebot.repo.DiscordUserRepository;
import at.lumi200.discordquotebot.repo.QuoteRepository;
import at.lumi200.discordquotebot.repo.ServerRepository;
import discord4j.common.util.Snowflake;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.ApplicationCommandInteractionOption;
import discord4j.core.object.command.ApplicationCommandInteractionOptionValue;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.util.Color;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

@Log4j2
@Component
public class QuoteCommand implements ChatInputCommand {
	@Autowired
	private QuoteRepository quoteRepository;
	@Autowired
	private DiscordUserRepository discordUserRepository;

	@Autowired
	private ServerRepository serverRepository;

	@Override
	public String getName() {
		return "quote";
	}

	@Override
	public Mono<Void> handle(ChatInputInteractionEvent event) {

		User discordTargetUser = null;
		Server discordServer;
		DiscordUser sourceUser;
		DiscordUser creatorUser;
		DiscordUser targetUser;
		MessageChannel quoteChannel;
		Quote quote;

		User user = event.getOption("user")
				.flatMap(ApplicationCommandInteractionOption::getValue)
				.map(ApplicationCommandInteractionOptionValue::asUser)
				.get().block();

		if (event.getOption("target").isPresent()) {
			discordTargetUser = event.getOption("target")
					.flatMap(ApplicationCommandInteractionOption::getValue)
					.map(ApplicationCommandInteractionOptionValue::asUser)
					.get().block();
		}


		long sourceId = user.getId().asLong();
		long creatorId = event.getInteraction().getUser().getId().asLong();
		long serverId = event.getInteraction().getGuildId().get().asLong();

		if (discordUserRepository.existsBySnowflake(sourceId))
			sourceUser = discordUserRepository.findBySnowflake(sourceId);
		else
			sourceUser = discordUserRepository.save(new DiscordUser(sourceId));

		if (discordUserRepository.existsBySnowflake(creatorId))
			creatorUser = discordUserRepository.findBySnowflake(creatorId);
		else
			creatorUser = discordUserRepository.save(new DiscordUser(creatorId));

		if (serverRepository.existsBySnowflake(serverId))
			discordServer = serverRepository.findBySnowflake(serverId);
		else
			discordServer = serverRepository.save(new Server(serverId));


		String quoteS = event.getOption("content")
				.flatMap(ApplicationCommandInteractionOption::getValue)
				.map(ApplicationCommandInteractionOptionValue::asString)
				.get();

		if (discordTargetUser == null)
			quote = Quote.builder()
					.quote(quoteS)
					.server(discordServer)
					.creator(creatorUser)
					.source(sourceUser)
					.created(LocalDateTime.now())
					.build();
		else {
			long targetId = discordTargetUser.getId().asLong();

			if (discordUserRepository.existsBySnowflake(targetId))
				targetUser = discordUserRepository.findBySnowflake(targetId);
			else
				targetUser = discordUserRepository.save(new DiscordUser(targetId));

			quote = Quote.builder()
					.quote(quoteS)
					.server(discordServer)
					.creator(creatorUser)
					.source(sourceUser)
					.target(targetUser)
					.created(LocalDateTime.now())
					.build();
			quoteRepository.save(quote);
		}
		DateTimeFormatter formatter = new DateTimeFormatterBuilder()
				.appendPattern("HH:mm dd.MM.YYYY").toFormatter()
				.withZone(ZoneId.of("Europe/Vienna"));

		quoteChannel = event.getClient().getChannelById(Snowflake.of(serverRepository.findBySnowflake(event.getInteraction().getGuildId().get().asLong()).getQuoteChannel())).cast(MessageChannel.class).block();

		if (quote.getTarget() == null)
			quoteChannel.getRestChannel().createMessage(
					EmbedCreateSpec.builder()
							.color(Color.BISMARK)
							.title(quote.getQuote())
							.author(user.getUsername(), "", user.getAvatarUrl())
							.description("%s".formatted(quote.getCreated().format(formatter)))
							.build().asRequest()).block();
		else
			quoteChannel.getRestChannel().createMessage(
					EmbedCreateSpec.builder()
							.color(Color.BISMARK)
							.title(quote.getQuote())
							.author(user.getUsername(), "", user.getAvatarUrl())
							.description("zu %s%n%s".formatted(discordTargetUser.getUsername(), quote.getCreated().format(formatter)))
							.build().asRequest()).block();
			return event.reply().withEphemeral(true).withContent("Quote saved!");
	}
}
