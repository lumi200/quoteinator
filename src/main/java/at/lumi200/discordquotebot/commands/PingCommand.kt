package at.lumi200.discordquotebot.commands

import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
class PingCommand : ChatInputCommand {
    override fun getName(): String {
        return "ping"
    }

    override fun handle(event: ChatInputInteractionEvent): Mono<Void> {
        return event.reply()
            .withEphemeral(true)
            .withContent("Pong!")
    }
}