package at.lumi200.discordquotebot.commands;

import at.lumi200.discordquotebot.entity.Quote;
import at.lumi200.discordquotebot.repo.DiscordUserRepository;
import at.lumi200.discordquotebot.service.QuoteService;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.ApplicationCommandInteractionOption;
import discord4j.core.object.command.ApplicationCommandInteractionOptionValue;
import discord4j.core.object.entity.User;
import discord4j.core.spec.EmbedCreateFields;
import discord4j.core.spec.EmbedCreateSpec;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class AllQuotesFromUserCommand implements ChatInputCommand {

	final QuoteService quoteService;
	final DiscordUserRepository userRepository;

	public AllQuotesFromUserCommand(QuoteService quoteService, DiscordUserRepository userRepository) {

		this.quoteService = quoteService;
		this.userRepository = userRepository;
	}

	@Override
	public String getName() {
		return "all-quotes-from-user";
	}

	@Override
	public Mono<Void> handle(ChatInputInteractionEvent event) {

		DateTimeFormatter formatter = new DateTimeFormatterBuilder()
				.appendPattern("HH:mm dd.MM.YYYY").toFormatter()
				.withZone(ZoneId.of("Europe/Vienna"));

		User user = event.getOption("user")
					.flatMap(ApplicationCommandInteractionOption::getValue)
					.map(ApplicationCommandInteractionOptionValue::asUser).orElseThrow()
					.block();

		List<Quote> quotes = quoteService.getAllFromSource(userRepository.findBySnowflake(user.getId().asLong()).getId());

		List<EmbedCreateFields.Field> fields = quotes.stream()
				.map(quote -> EmbedCreateFields.Field.of(quote.getQuote(), quote.getCreated().format(formatter), false))
				.collect(Collectors.toList());

		return event.reply().withEmbeds(EmbedCreateSpec.builder()
				.author(user.getUsername(), "", user.getAvatarUrl())
				.addAllFields(fields)
				.build());

	}
}
