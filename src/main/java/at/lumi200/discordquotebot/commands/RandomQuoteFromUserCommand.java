package at.lumi200.discordquotebot.commands;

import at.lumi200.discordquotebot.entity.Quote;
import at.lumi200.discordquotebot.repo.DiscordUserRepository;
import at.lumi200.discordquotebot.repo.QuoteRepository;
import at.lumi200.discordquotebot.service.QuoteService;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.ApplicationCommandInteractionOption;
import discord4j.core.object.command.ApplicationCommandInteractionOptionValue;
import discord4j.core.object.entity.User;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.RestClient;
import discord4j.rest.util.Color;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

@Component
public class RandomQuoteFromUserCommand implements ChatInputCommand {

	private final QuoteService quoteService;
	private final RestClient client;
	private final DiscordUserRepository discordUserRepository;


	public RandomQuoteFromUserCommand(QuoteService quoteService, RestClient client,
									  DiscordUserRepository discordUserRepository) {
		this.quoteService = quoteService;
		this.client = client;
		this.discordUserRepository = discordUserRepository;
	}

	@Override
	public String getName() {
		return "find-random-quote";
	}

	@Override
	@Transactional(readOnly = true)
	public Mono<Void> handle(ChatInputInteractionEvent event) {

		User user;
		Quote quote;
		User target = null;

		if (event.getOption("user").isPresent()) {

			user = event.getOption("user")
					.flatMap(ApplicationCommandInteractionOption::getValue)
					.map(ApplicationCommandInteractionOptionValue::asUser)
					.get().block();

			if (discordUserRepository.existsBySnowflake(user.getId().asLong()))
				quote = quoteService.getRandomFromSource(discordUserRepository.findBySnowflake(user.getId().asLong()).getId(), event.getInteraction().getGuildId().get().asLong());
			else
				quote = null;

			if (quote == null)
				return event.reply()
						.withEphemeral(true)
						.withContent("No Quote from %s found".formatted(user.getMention()));

		} else {
			quote = quoteService.getRandom(event.getInteraction().getGuildId().get().asLong());
			if (quote == null)
				return event.reply()
						.withEphemeral(true)
						.withContent("No quotes found, create one with /quote");
			user = new User(event.getClient(), client.getUserService().getUser(quote.getSource().getSnowflake()).block());
		}
			if (quote.getTarget() != null)
				target = new User(event.getClient(), client.getUserService().getUser(quote.getTarget().getSnowflake()).block());

		DateTimeFormatter formatter = new DateTimeFormatterBuilder()
				.appendPattern("HH:mm dd.MM.YYYY").toFormatter()
				.withZone(ZoneId.of("Europe/Vienna"));
		if (quote.getTarget() == null)
			return event.reply().withEmbeds(EmbedCreateSpec.builder()
					.color(Color.BLUE)
					.title(quote.getQuote())
					.author(user.getUsername(), "", user.getAvatarUrl())
					.description("%s".formatted(quote.getCreated().format(formatter)))
					.build());
		else
			return event.reply().withEmbeds(EmbedCreateSpec.builder()
					.color(Color.BLUE)
					.title(quote.getQuote())
					.author(user.getUsername(), "", user.getAvatarUrl())
					.description("zu %s%n%s".formatted(target.getUsername(), quote.getCreated().format(formatter)))
					.build());
	}
}
