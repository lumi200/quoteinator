package at.lumi200.discordquotebot.commands;

import at.lumi200.discordquotebot.entity.Quote;
import at.lumi200.discordquotebot.entity.Server;
import at.lumi200.discordquotebot.repo.ServerRepository;
import at.lumi200.discordquotebot.service.QuoteService;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.ApplicationCommandInteractionOption;
import discord4j.core.object.command.ApplicationCommandInteractionOptionValue;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.channel.Channel;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.core.spec.InteractionFollowupCreateSpec;
import discord4j.rest.RestClient;
import discord4j.rest.util.Color;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;

@Component
@Log4j2
public class SetupCommand implements ChatInputCommand {

	private ServerRepository serverRepository;

	private RestClient client;

	private QuoteService quoteService;

	public SetupCommand(ServerRepository serverRepository, RestClient client, QuoteService quoteService) {
		this.serverRepository = serverRepository;
		this.client = client;
		this.quoteService = quoteService;
	}


	@Override
	public String getName() {
		return "setup";
	}

	@Override
	public Mono<Void> handle(ChatInputInteractionEvent event) {

		MessageChannel channel = event.getOption("channel")
				.flatMap(ApplicationCommandInteractionOption::getValue)
				.map(ApplicationCommandInteractionOptionValue::asChannel)
				.get().cast(MessageChannel.class).block();

		event.reply().withEphemeral(true).withContent("Setting up channel, can take some time").block();

		log.debug(event.getInteraction().getGuildId().get().asLong());
		Server server = serverRepository.findBySnowflake(event.getInteraction().getGuildId().get().asLong());
		if (server == null)
			server = serverRepository.save(new Server(event.getInteraction().getGuildId().get().asLong()));

		server.setQuoteChannel(channel.getId().asLong());

		serverRepository.save(server);

		setupChannel(event.getInteraction().getGuildId().get().asLong(), channel, event);

		event.createFollowup(InteractionFollowupCreateSpec.builder().ephemeral(true).content("Setup finished").build()).block();

		return Mono.empty();
	}

	private void setupChannel(Long serverId, Channel quoteChannel, ChatInputInteractionEvent event) {

		List<Quote> quotes = quoteService.getAllFromServer(serverId);

		quotes.forEach(quote -> {
			User user = new User(event.getClient(), client.getUserService().getUser(quote.getSource().getSnowflake()).block());
			User target = null;
			if (quote.getTarget() != null)
				target = new User(event.getClient(), client.getUserService().getUser(quote.getTarget().getSnowflake()).block());
			DateTimeFormatter formatter = new DateTimeFormatterBuilder()
					.appendPattern("HH:mm dd.MM.YYYY").toFormatter()
					.withZone(ZoneId.of("Europe/Vienna"));
			if (quote.getTarget() == null)
				quoteChannel.getRestChannel().createMessage(
						EmbedCreateSpec.builder()
								.color(Color.BISMARK)
								.title(quote.getQuote())
								.author(user.getUsername(), "", user.getAvatarUrl())
								.description("%s".formatted(quote.getCreated().format(formatter)))
								.build().asRequest()).block();
			else
				quoteChannel.getRestChannel().createMessage(
						EmbedCreateSpec.builder()
								.color(Color.BISMARK)
								.title(quote.getQuote())
								.author(user.getUsername(), "", user.getAvatarUrl())
								.description("zu %s%n%s".formatted(target.getUsername(), quote.getCreated().format(formatter)))
								.build().asRequest()).block();
		});

	}
}
