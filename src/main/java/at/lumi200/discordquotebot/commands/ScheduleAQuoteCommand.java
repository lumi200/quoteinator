package at.lumi200.discordquotebot.commands;

import at.lumi200.discordquotebot.TaskDefinition;
import at.lumi200.discordquotebot.TaskDefinitionBean;
import at.lumi200.discordquotebot.TaskDefinitionService;
import at.lumi200.discordquotebot.repo.ServerRepository;
import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.ApplicationCommandInteractionOption;
import discord4j.core.object.command.ApplicationCommandInteractionOptionValue;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.rest.RestClient;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Locale;
import java.util.UUID;

@Log4j2
@Component
@AllArgsConstructor
public class ScheduleAQuoteCommand implements ChatInputCommand {


	@Override
	public String getName() {
		return "schedule-a-quote";
	}

	private TaskDefinitionService taskDefinitionService;

	private TaskDefinitionBean taskDefinitionBean;

	private RestClient client;

	private ServerRepository serverRepository;

	@Override
	public Mono<Void> handle(ChatInputInteractionEvent event) {

		String cronExpression = event.getOption("cron")
				.flatMap(ApplicationCommandInteractionOption::getValue)
				.map(ApplicationCommandInteractionOptionValue::asString)
				.get();

		log.debug(cronExpression);
		if (!CronExpression.isValidExpression(cronExpression))
			return event.reply().withEphemeral(true).withContent("Invalid Cron expression: See https://www.cronmaker.com/");

		MessageChannel channel = event.getOption("channel")
				.flatMap(ApplicationCommandInteractionOption::getValue)
				.map(ApplicationCommandInteractionOptionValue::asChannel)
				.get().cast(MessageChannel.class).block();


		taskDefinitionBean.setTaskDefinition(new TaskDefinition(channel));
		taskDefinitionService.scheduleATask(
				UUID.randomUUID(),
				taskDefinitionBean,
				cronExpression,
				serverRepository.findBySnowflake(event.getInteraction().getGuildId().get().asLong()),
				channel.getId().asLong());

		CronDescriptor descriptor = CronDescriptor.instance(Locale.UK);
		CronParser parser = new CronParser(CronDefinitionBuilder.instanceDefinitionFor(CronType.SPRING));


		return event.reply().withEphemeral(true).withContent("Quotes are now scheduled in %s for %s".formatted(
				channel.getRestChannel().getData().block().name().get(),
				descriptor.describe(parser.parse(cronExpression))));
	}
}
